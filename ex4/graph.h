#pragma once
//Graph implementation as adjacancy list
#include <vector>
#include <list>
#include "io.h"
#include <iomanip>

namespace exercise{
    
            
    class graph{ //how do I properly implement this shiiiish
        public: //data structures
        struct arrow {
            int vi;
            int vj;
            int weight;
            arrow()=default;
            arrow(int i, int j, int w) : vi(i), vj(j), weight(w) {};
            };
            const int NOT_FOUND=99999;
            
        private:
            int X; //Starting vertex
            int Y;  //Goal/Ending vertex
            unsigned int numVert; //Vertecies
            unsigned int numArr; //Arrows
            std::vector<std::vector<arrow> > vertex;
            using vertvec = std::vector<std::list<arrow> >;
            std::vector<std::vector<int> > path;
            
        public:
            void trytest(arrow a);
            graph(unsigned int size) : numVert(int(size)), vertex(size) {}; //constructor
            graph();
            ~graph()=default;
            void addArrow(int i, int j, int w); //Add weighted arrow
            /*bool isConnected(int vi, int vj); //Check if arrow exists (maybe unneeded)
            std::list<int> getPointTo(int i); //get the vertecies the vertex_i is pointing to
            std::list<int> getPointFrom(int i); //get verticies that are pointing to vertex_i
            unsigned int size(); //get total size of graph [# of vertex]
            void operator=(const arrow &a); //copy and comparision operator (i couldnt do it void)*/  //unused function ideas
            
            void getInput(); //grab the input from console
            void printSol(); //Prints solution to the problem
            friend std::ostream;
            friend exercise::io;            
            void printarrow(); 
            void printGraph();//Print graph to console
            int getStart();
            int getEnd();
            
            //algorithm
            int lightest_path_algorithm();
            void print_all_paths();
            void official_print();
            

    };
}