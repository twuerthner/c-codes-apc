#include "graph.h"

namespace exercise{
    void graph::printarrow(){
      arrow arrow(1, 2, 3);
      std::cout<<arrow.vi<<" "<<arrow.vj<<" "<<arrow.weight<<std::endl;
    }
    
    void graph::addArrow(int vi, int vj, int w){
      arrow new_arrow(vi, vj, w);
      vertex[vi].push_back(new_arrow);
    }//Add new arrow to graph
    
    void graph::getInput(){
        graph::arrow myArrow;
        std::cin >> numArr;
        std::cin >> X;
        std::cin >> Y;
        for(unsigned int n=0; n < numArr; n++){
            std::cin >> myArrow.vi >> myArrow.vj >> myArrow.weight;
            if(myArrow.vi >= int(numVert) || myArrow.vj >= int(numVert))
            throw std::length_error("Error: Arrows poinitng to nonexistent vertecies");
            vertex[myArrow.vi].push_back(myArrow);
        } //loop over all incoming input vertex
    }
    
    void graph::printGraph(){// I do need a copy cause im going to pop it all
      std::cout << "number of verteces: "<< numVert <<std::endl;
      std::cout << "number of arrows: " << numArr << std::endl;
      graph tmp(numVert);
      tmp.vertex = vertex;
      for(unsigned int i=0; i<tmp.numVert; i++){
        while(!tmp.vertex[i].empty()){
          std::cout << "Arrow from " << i << " pointing to " << tmp.vertex[i].back().vj <<" with weight " << tmp.vertex[i].back().weight <<std::endl;
          tmp.vertex[i].pop_back();//after learning of the vector.size() function this is a very stupid implementation but I can't be bothered to change it. Next time ill just do the array-like implementation
        }
      }  
    }
  
  int graph::lightest_path_algorithm(){
    std::vector<int> distance;
    distance.resize(numVert);// create vector carrying distances
    path.resize(numVert);
    for(unsigned int i=0; i<numVert; i++){
      distance[i]=NOT_FOUND;
      path[i].push_back(X);
    }//set all distances to infinite
    distance[X]=0; //starting vertex
    for(unsigned int ALGORITHM_ITR=0; ALGORITHM_ITR<numVert; ALGORITHM_ITR++){ //could be more efficient if I checked when nothing changes for very big graphs
      for(unsigned int i=0; i<numVert; i++){
        unsigned int vertex_size=vertex[i].size();
        if(distance[i]!=NOT_FOUND){//if distance != infinity, consider the vertex
          for(unsigned int n=0; n<vertex_size; n++){
            int new_dist = distance[i]+vertex[i][n].weight;
            if(new_dist<distance[vertex[i][n].vj]){
              distance[vertex[i][n].vj]=new_dist; 
              path[vertex[i][n].vj]=path[i];
              path[vertex[i][n].vj].push_back(vertex[i][n].vj);
            }
          }//if the new distance from vertex i is better than whatever the current distance of vj is, overwrite
        }//if the distance is not infinite, consider the vertex
      }//loop over all the vertecies
    }
    return distance[Y];
  }
  
  void graph::print_all_paths(){
    for(unsigned int i=0; i<numVert; i++){
      std::cout << "The path to vertex " << i << " is: ";
      for(unsigned int n=0; n<path[i].size(); n++){
        std::cout << path[i][n] << " ";
      }
      std::cout << std::endl;
    }
  }
  
  void graph::official_print(){//i dislike the order of official printing
    for(int n=int(path[Y].size())-1; n>=0; n--){
        std::cout << path[Y][n] << " ";
      }
      std::cout << std::endl;
  }
  
  int graph::getStart(){return X;}
  
  int graph::getEnd(){return Y;} //i hope you don't mind but I like to compactify these super simple functions
    
}
