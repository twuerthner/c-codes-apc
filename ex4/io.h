#pragma once
#include <vector>
#include <list>
#include <iostream>

namespace exercise{
    class io{//I didn't get this to work before losing my sanity on that day, so I did all in the graph class
        
        public:
        
            static unsigned int numVert; //Vertecies
            static unsigned int numArr; //Arrows
            static int StartPt; //Starting vertex
            static int EndPt; //Goal/Ending vertex

             static void getInput(); //grab the input from console
             void printGraph();//Print graph to console
             void printSol(); //Prints solution to the problem
    };
}