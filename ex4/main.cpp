#include<iostream>
#include<string>
#include<iomanip>
#include<vector>
#include<algorithm>

#include"graph.h"
#include"io.h"
#include"algorithm.h"

#include"graph.cpp"
#include"io.cpp"
#include"algorithm.cpp"

using namespace exercise;





int main(){
  
    int V; std::cin >> V; graph g(V); g.getInput(); //make the graph
    
    int distance = g.lightest_path_algorithm(); //get distance from Starting vertex to final vertex
    
    if(distance==g.NOT_FOUND) std::cout << "ERROR" << std::endl; //output the weight
    
    else std::cout << distance << std::endl; 
    
    g.official_print(); //print out the path
    
    return 0;
}