#pragma once //include guard
#include "elements.h"

namespace exercises
{
  /****************************
   * CLASS NODE
  ***********************/
  // the hashtable is organized as an array of linked lists.
  // the linked lists are composed of nodes, each node has
  // a pointer to the actual data: element *item.
  class node
  {
  public:
    element *item = nullptr;
    
    // pointer to the next node, NULL if this is the last one.
    node *next = nullptr;

    // CONSTRUCTOR 0
    // default constructor
    node(); 
    node(const node &) = delete; //using copy constructor not allowed!
    node &operator=(const node &) = delete; //remove 
    
    // CONSTRUCTOR I
    // constructor that inserts an element at the end
    node( element *element );
    
    // CONSTRUCTOR II
    // constructor to insert element at any position in the linked list
    node( element *element, node *node );

    // DESTRUCTOR
    // destructor to delete the item in the end.
    ~node()=default;
  };
}