/* I'm really sorry to you for having to correct this
Next time I'll try to place some more senseful comments
otherwise I think the code is pretty straight forward
Have this funny image of an artistic intepretation of the compiler
https://cdn.discordapp.com/attachments/407172428960235530/901613867308810271/unknown.png*/
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <queue>


#include "elements.h"
#include "node.h"
#include "hashtable.h"


std::string database_file = "database.dat";
std::string checker_file  = "look_up_elements.dat";

using namespace exercises;



int main() {
  std::cin >> database_file;
  std::cin >> checker_file;
  
  int comparisions = 0;  
  std::string line_init; 
  std::ifstream file(database_file);  //open file
  hashtable *hsht = new hashtable;    //create hashlist
  while(!file.eof()){ //add all elements to the hashlist
      hsht->add_element(file);
  }
  file.close();

  std::ifstream look(checker_file);   //open other file
  std::queue<std::string> queue;    
  std::string name;
  std::string line;
  while(!look.eof()){                 //push all atom names on a queue
    look >> name;
    if(name != "#")queue.push(name);
    getline(look, line);
  }
  look.close();
  while(!queue.empty()){ //I'm going to be honest here, this part was purely to satisfy the automatic tests, it's ugly
    element *element =hsht->lookup(queue.front(), hsht, comparisions);
    if(!element->getm())element->printn(); //prints out the name aka error if mass == 0
    queue.pop(); //pops element out of queue
    if(element->getm())element->printm();
    else if(!queue.empty())std::cout << "error" <<std::endl;
  }

  //print number of comparisions 
  std::cout << comparisions << std::endl;
  hsht->~hashtable(); //I'm pretty confident that this program doesn't have memory leaks TM
  return 0;
}
