#include "hashtable.h"
#include <iomanip>

namespace exercises{

      //raw pointers -> manual data managment 
      hashtable::hashtable(){ 
        for(unsigned int n=0; n<max_hashsize; n++){
          table[n]=nullptr; //create table pointing to only nullptrs
        }
      }

      hashtable::~hashtable(){
        node *tmp_node;
        for(unsigned int n=0; n<max_hashsize; n++){
          tmp_node = table[n]; //get the address to first node of hashtable
          while(tmp_node->next != nullptr){
            node *tmp_deleter = tmp_node;
            tmp_node = tmp_node->next;//we go to the next node 
            tmp_deleter->~node();
          }
          tmp_node->~node();
        }
      }

      void hashtable::add_element(std::ifstream &file){ //add an element from a file stream
       element *elmnt = new element(file);    //create new element
       int hash = elmnt->hash();             //calc hash once
       node *nde = new node(elmnt, table[hash]);     //create new node with address of next node and of element it's pointing to
       (table[hash]=nde);   //assign address of new node to hash table
      } 

      void hashtable::add_element(const std::string &iname, double imass){ // add an element manually
        element *elmnt = new element(iname, imass);    //create new element
        int hash = elmnt->hash();                     //calc hash once
        node *nde = new node(elmnt, table[hash]);    //create new node
        (table[hash]=nde);   //assign address of new element to hash table
      } 

      element* hashtable::lookup(const std::string &name, hashtable *hashtable, int &compar){ // find a element in hashtable and count string comparisons
        element *esearch = new element(name);   //create element with searched for name
        int hash = esearch->hash();           //calc hash
        node *tmp_node = hashtable->table[hash]; //get the address to first node
        while(true){
          if(tmp_node==nullptr){          //if the address is a nullptr, we're at the last node and return error
            element *errorelement = new element("error");
            return errorelement;
          } 
          compar++;   //increment comparison counter
          if(tmp_node->item->get_name() == esearch->get_name()){  //if we found the element, return its address (didnt get the operator overloading to work for elements)
            return tmp_node->item;
          }
        tmp_node = tmp_node->next;    //we go to the next node
        }
      }
      
      void hashtable::print(){  // Prints out hashtable
        node *tmp_node;
        for(unsigned int n=0; n<max_hashsize; n++){
        tmp_node = table[n]; //get the address to first node
        while(tmp_node->next != nullptr){
          tmp_node = tmp_node->next;    //we go to the next node
          std::cout << std::setw(10)<< tmp_node->item->get_name() <<" at "<< tmp_node->item <<  " and location of next node is:  " << tmp_node->next << std::endl; //Debugging
          }
        }
      }
}