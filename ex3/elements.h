#pragma once //include guard 
#include <string>
#include <sstream>
#include <fstream>

namespace exercises
{
  /****************************
   * CLASS ELEMENT
  ***********************/
  class element
  {
    private:
      // the name
      std::string name = "";
      // and the mass
      double mass = 0.0;
      
    public:
      // no default constructor?
      element() = default;
      // default arguments...
      element( const std::string &iname, double imass = 0 ) : name(iname), mass(imass) {};
      
      // another useful constructor
      element(std::ifstream &file );
      
      // hash the name
      unsigned int hash(unsigned int max = 5, unsigned int factor = 101);
      
      // operator overloading!
      bool operator==( const element &e );

      // print out the thing
      void print();
      
      // print mass
      void printm();

      // print name
      void printn();
      
      // get name of element
      std::string get_name();
       //gets mass of element
      double getm();
    };
} /* end of namespace exercise */