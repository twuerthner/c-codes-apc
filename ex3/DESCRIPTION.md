# Exercise 3 - Hash-table
In this exercise, you should implement a hash-table and the corresponding hash-function to look up the mass of elements. Your program should (1) read in a file, (2) store the information in instances of the class ``element``, (3) calculate a hash value from the name and (4) store the respective instance of the class at the appropriate position in the hash-table. Then it should read in a second file called ``look_up_elements.dat``, and lookup the mass of the element from the hash-table. During this lookup, you should count the number of string comparisons needed. The program should print first the masses of the elements found, then the number of comparisons needed, separated by newlines. If the element searched for is not in the database, then the code should print out "error". 13 Points are awarded, if the test sets are solved with an hash-table implementation and if the code utilizes linked lists. For this, implement the functions provided in the header file.

**Input Format**

First input is a string $s_1$, a file name.
Second input is a string $s_2$, a file name.
The file $s_1$ has the following format:

    # name mass
    Zinc 65.37
    Hydrogen 1.08
    Carbon 12.011
    ... ...

The file $s_2$ has the following format

    # name 
    Xenon
    Zinc
    Uran
    ...

**Constraints**

Use following code as a hash function:

    unsigned int hash( unsigned int max, unsigned int factor) const {
        auto sum = 0u;
        for(auto chr : name){
            sum += static_cast<unsigned int>(chr) * factor; 
        sum %= max;
        }
        return sum;
    }
  
Use the constants ``MAX=5`` and ``FACTOR=101``. This is important to get the correct number of string comparisons!

**Output Format**

$n$ double's $m$, the mass of each element in the second input file.
An integer $c$, the number of total string comparisons.
Each output is separated by a newline.

|Sample Input|Sample Output|
|------------|-------------|
|database.dat|             |
|look_up_elements.dat|131.3|
|            |  65.37      |
|            |  error      |
|            |   8         |