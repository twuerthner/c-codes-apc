#pragma once
#include "node.h"
#include "elements.h"
#include "iostream"

namespace exercises
{
  /****************************
   * CLASS HASHTABLE
  ***********************/
  //
  class hashtable
  {
    public:
      static const unsigned int max_hashsize = 5; //the size of the table
      unsigned int num_comparisons = 0u; //control value for the exercis
    
      node* table[max_hashsize]; // the actual datastructure
    
      //raw pointers -> manual data managment 
      hashtable();
      ~hashtable();
      hashtable(const hashtable &) = delete;
      hashtable &operator=(const hashtable &) = delete;
    
      void add_element(std::ifstream &file); //add a element froma file stream
      void add_element(const std::string &iname, double imass = 0); // add a element manually
      void print(); //for debugging not required for the solution
      element* lookup(const std::string &name, hashtable *hashtable, int &compar); // find a element in hashtable and count string comparisons
  };
}