#include "node.h"
#include "elements.h"
namespace exercises{
    node::node(){} //default constructor


    node::node(element *element){
        item = element;
    }

    node::node(element *element, node *node){
        item = element;
        next = node;
    }
}