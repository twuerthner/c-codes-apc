namespace exercise {
  class Molecule;
  
  class Algorithm{
    protected:
    	Molecule molecule;
    	
      Algorithm()=default;
    	void initInvariants();
    	void generateInvariants();
      //void updateInvariants();

    public:
      //Algorithm functions
    	virtual int findInvariants() = 0; //pure virtual functions
      virtual void setRanks() = 0;
      
      //get&set
    	Molecule getMolecule();
    	void setMolecule(Molecule mol);
    	      
  };
  
  class Test{
    public:
    int test_int = 0;
    Test();

    int findInvariants();
    void setRanks();
  };

  class Morgan: public Algorithm { //public Algorithm
    public:
      Morgan()=default;
      Morgan(Molecule mol);

      //Algorithm
      
    	int findInvariants() override;
      void setRanks() override;
      
      //Func<T1, TResult> myVar = c => _configuration = c;
      
  };
  
  class MorganAdapted: public Algorithm { //public Algorithm
  public:
    MorganAdapted(Molecule mol);
    
    //Algorithm
  	int findInvariants();
    void setRanks();
  };

}