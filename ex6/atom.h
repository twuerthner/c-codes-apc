#include <iostream>
#include <vector>
#include <map>
#include <iterator>
#include <algorithm>
#include <set>

namespace exercise {
  class Atom{
  private:
    int id;
  	int invariant=0;
  	std::vector<int> adjacents;
  	int rank=0;
  	int newInvariant=0;
  	
  public:
  	Atom(int id, const std::vector<int>& adjacents_) ;
  	

  	bool operator<(const Atom& other) const;
  	bool operator>(const Atom& other) const;

  	
    //get set
    const std::vector<int> &getAdjacents() const;
    int getInvariant() const;
    void setInvariant(int inVariant);
    int getNewInvariant() const ;
    void setNewInvariant(int inVariant);
    int getRank() const;
    void setRank(int rank);
    int getID() const;
    
    //Print
  	void printInvariant() const;
  	
    //friends
  	friend class Molecule;
  };
}