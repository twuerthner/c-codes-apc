#include <iostream>
#include <string>
#include <algorithm>
#include <set>


#include "molecule.h"
#include "atom.h"


//using namespace exercise;

namespace exercise {
//Constructor
  Molecule::Molecule(){}
	Molecule::Molecule(const std::vector<Atom> &atoms){
		this->atoms = atoms;
	}

//PUBLIC
  int Molecule::findNextAtom(std::vector<Atom> &tmp_atoms){//find max_pos of atom with smallest label, that has unlabled neighbors (0 is unlabled and doesn't count)
    auto sort_tmp_atoms = tmp_atoms; //passes by reference, copies: nice
    sort(sort_tmp_atoms.begin(), sort_tmp_atoms.end(), [](Atom a, Atom b){
      return a.getRank() < b.getRank();
    });//sorts by Rank
    
    for(int n = 0; n < sort_tmp_atoms.size(); n++){//loop over the sorted list of all atoms starting with the one with biggest invariant
      for( int k = sort_tmp_atoms[n].getAdjacents().size()-1; k >= 0; k--){//loop over the neighbors k of n
      if(sort_tmp_atoms[n].getRank()!= 0 && tmp_atoms[sort_tmp_atoms[n].getAdjacents()[k]].getRank()==0) return sort_tmp_atoms[n].getID(); //if atom n has a non zero rank and is the lowest labled (by the sorting) and at least one of its neighbours have a unlabled rank, return atom n's ID as max_pos 
      }
    }
	}
	
  bool Molecule::existsUnlabeledAtoms() const{
  	for(int n = 0; n < atoms.size(); n++){
      if(atoms[n].getRank()==0) return true;
    }
    return false;
  }

	//getter && setter
	int Molecule::getNumInvariants() const{
    int num_inv = 1;
    int num_atms= this->getAtoms().size(); //get number of atoms
    std::vector<int> invars;
    for(int i = 0; i < num_atms; i++) invars.push_back(this->atoms[i].getInvariant());  //create vector with all the invariants
    std::sort(invars.begin(), invars.end());                //sort the invariants                     FLAG FLAG FLAG possible error source
    //invars.erase(std::unique(invars.begin(), invars.end()));//delete duplicate invariants
    //invars.shrink_to_fit();                                 //shrink to fit the right size
    auto ITR = num_atms-1;
    /*for(int l = 0; l < num_atms; l++){
      std::cout << "invars in getnuminvars "<< invars[l] << std::endl;
    }*/
    for(int l = 0; l < ITR; l++){
      if(invars[l]!=invars[l+1]) num_inv++;
    }
    return num_inv;
  }

	const std::vector<Atom> &Molecule::getAtoms() const{
	  return atoms; 
	}
 std::vector<Atom> &Molecule::getAtoms() {
    return atoms; 
  }
	void Molecule::setAtoms(const std::vector<Atom> &atoms){
	  this->atoms = atoms;
	}
  void Molecule::setAtom(const Atom &Atm, const int &a){
    this->atoms[a] = Atm;
  }
  //print functions:
  void Molecule::printAdjacents() const
  {
    std::cout << "AtomsAdjencents: " << std::endl;
    std::cout << "#\tAtom \t Neighbours (1 ... N)" << std::endl;
    
    for(const auto &atom : atoms)
    {
      std::cout << "\t" << atom.id << "\t ";
      for(auto adjen : atom.getAdjacents()){
        std::cout << " " << adjen ; 
      }
      std::cout << std::endl;
    }
  }
  void Molecule::printInvariants() const
  {
    int counter=0;
    std::cout << "Invariants: " << std::endl;
    std::cout << "#\t Atom\t Invariant"<<std::endl;
    for(const auto& atom : atoms)
    {
      std::cout << "\t" << counter << "\t" << atom.getInvariant() << std::endl;
      counter++;
    }
  }
  void Molecule::printRanks() const
  {
    int counter=0;
    std::cout << "Ranks: " << std::endl;
    std::cout << "#\tAtom  Rank"<<std::endl;
    for(const auto& atom : atoms)
    {
      std::cout << "\t" << counter  << "\t" << atom.getRank()<<std::endl;
      counter++;
    }
  }

  void Molecule::setRanks(){
    
    return;
  }
}
