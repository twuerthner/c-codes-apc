#include <vector>

namespace exercise {
  class Atom;
  class Molecule{
  private:
  	std::vector<Atom> atoms;
  	//bool isNumInvSame();
  	
  public:
  	//Constructor
    Molecule();
  	Molecule(const std::vector<Atom> &atoms);

  	//funcs
  	int findNextAtom(std::vector<Atom> &tmp_atoms);
    bool existsUnlabeledAtoms() const;
    
  	//get & set
  	const std::vector<Atom> &getAtoms() const;
    std::vector<Atom> &getAtoms();
  	void setAtoms(const std::vector<Atom> &atoms);
	void setAtom(const Atom &Atm, const int &a);
  	int getNumInvariants() const;
  	void setRanks();
  	
  	void printAdjacents() const;
  	void printInvariants() const;
  	void printRanks() const;

  };
}