#include <vector>
#include "atom.h"


namespace exercise{
  Atom::Atom( int id, const std::vector<int>& adjacents_) : id(id),  adjacents(adjacents_)
  {}
  
  bool Atom::operator<(const Atom& other) const
  {
  	return (invariant < other.getInvariant());
  }
  bool Atom::operator>(const Atom& other) const
  {
  	return (invariant > other.getInvariant());
  }
  
  //Adjacents
  const std::vector<int> &Atom::getAdjacents() const{
    return adjacents;
  }
  
  //Invariants
  int Atom::getInvariant() const {
    return invariant;
  }
  void Atom::setInvariant(int inVariant){
    this->invariant = inVariant;
  }
  //Atoms
  int Atom::getRank() const{
    return rank;
  }
  void Atom::setRank(int rank){
    this->rank = rank;
  }
  void Atom::printInvariant() const
  {
  	std::cout << invariant << std::endl;
  }
  int Atom::getNewInvariant() const{
    return newInvariant;
  }
  void Atom::setNewInvariant(int inVariant){
    this->newInvariant = inVariant;
  }
  int Atom::getID() const{
    return id;
  }
}