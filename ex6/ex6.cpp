#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>

#include "molecule.h"
#include "atom.h"
#include "algorithm.h"

using namespace exercise;

namespace exercise{
  Molecule loadMolecule(std::string filename)
  {
  	std::ifstream fin(filename);
  	std::string tmpLine;
  	std::vector<Atom> atoms;
    int id = 0;
  	while(std::getline(fin,tmpLine))
  	
  	{
  	  if(tmpLine.find('#')==0){
  	    continue;
	  }
  	  
		std::vector<int> tmpAdjacents;
		std::istringstream sstream(tmpLine);
		int tmpInt;
		while(sstream >> tmpInt)
		{
			tmpAdjacents.push_back(tmpInt);
		}

		Atom atom(id, tmpAdjacents);
		atoms.push_back(atom);
		id++;
  	}
  	return Molecule(atoms);
  }
  std::string spacer="////////////////////////////////////////////////////////////////////////";
}

int main()
{
  std::string filename = "./testMolecule3.dat";
  std::cin >> filename; // for tests
  Molecule molec = loadMolecule(filename);
	Morgan Freeman = Morgan(molec);
	Freeman.findInvariants();
	Freeman.setRanks();
	molec = Freeman.getMolecule();
	molec.printInvariants();
	molec.printRanks();
  return 0;
}
