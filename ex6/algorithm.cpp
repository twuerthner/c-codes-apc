#include "atom.h"
#include "molecule.h"
#include "algorithm.h"
#include <algorithm>
#include <bits/stdc++.h>

//using namespace exercise;

namespace exercise{

void Algorithm::initInvariants(){
    int num_atoms = molecule.getAtoms().size();         //get number of atoms
    std::vector<Atom> tmp_atoms = molecule.getAtoms();  //get a tmp copy of the molecule
    for(int i = 0; i < num_atoms; i++){                 //go over all atoms
    tmp_atoms[i].setInvariant(molecule.getAtoms()[i].getAdjacents().size()); //set number of invariants to amount of bonds atom has
    }
    molecule.setAtoms(tmp_atoms);                       //set the atoms to tmp atoms
    return;
}

void Algorithm::generateInvariants(){                   
    int num_atoms = molecule.getAtoms().size();         //get number of atoms in mol.
    std::vector<Atom> tmp_atoms = molecule.getAtoms();  //
    int num_inv = molecule.getNumInvariants();          //get number of invariants
    int old_num_inv = -1;                               //
    std::vector<Atom> tmp_next_atoms = tmp_atoms;
    while(num_inv != old_num_inv){          //generate new invariants until # of invariants doesn't change                 
        old_num_inv = num_inv; 
        for(int i = 0; i < num_atoms; i++){ //loop over all atoms
            int inv = 0;
            int num_adj = tmp_atoms[i].getAdjacents().size(); //this is like 2 function calls so should be reasonably more efficient this way
            for(int k = 0; k < num_adj; k++){                  //loop over all neighbors of an atom
                inv += tmp_atoms[tmp_atoms[i].getAdjacents()[k]].getInvariant();
                //std::cout << inv <<": is the value of inv with k = "<< k <<" and tmp_atoms[i].getAdjacents()[k] = "<<tmp_atoms[i].getAdjacents()[k]<<"\n";
            }
            tmp_next_atoms[i].setInvariant(inv);
        }
        molecule.setAtoms(tmp_next_atoms);   //update the molecule with the current atoms
        num_inv = molecule.getNumInvariants();  //set the new number of invariants
        if(num_inv == old_num_inv) molecule.setAtoms(tmp_atoms); //set to previous version of atoms if we're done
        tmp_atoms = tmp_next_atoms;
    }

    return;
}

Molecule Algorithm::getMolecule(){
    return molecule;
}

void Algorithm::setMolecule(Molecule mol){
    molecule = mol;
    return;
}

//~~~~~~~~~~~~~~~~~ MORGAN ZONE ~~~~~~~~~~~~~~~~~

Morgan::Morgan(Molecule mol){
    setMolecule(mol);
}

int Morgan::findInvariants(){
    int num_inv = 0;
    initInvariants();
    generateInvariants();
    return num_inv;
}

void Morgan::setRanks(){
    int max_pos;
    int max = 0;
    std::vector<Atom> tmp_atoms = molecule.getAtoms();
    int num_atm = tmp_atoms.size();
    for(int i = 0; i < num_atm; i++){//set all Ranks to 0
        tmp_atoms[i].setRank(0);
    }
    for(int i = 0; i < num_atm; i++){//find biggest invariant (stable)
        if(tmp_atoms[i].getInvariant() > max) max = tmp_atoms[i].getInvariant();
    }
    for (int i = 0; i < num_atm; i++){//find position of biggest invariant
        if(max == tmp_atoms[i].getInvariant()) max_pos = i;
    }
    std::cout<< "max_pos is: "<< max_pos << "\n";
    tmp_atoms[max_pos].setRank(1); //set first Rank
    int Curr_Rank = 2;
    std::vector<Atom> sort_tmp_atms = tmp_atoms;
    std::sort(sort_tmp_atms.begin(), sort_tmp_atms.end());
    while(molecule.existsUnlabeledAtoms()){
        //for(int n = 0; n < tmp_atoms.size(); n++) std::cout << "ID: " << tmp_atoms[n].getID() <<" number in vector: "<<n<<"\n";
        molecule.printRanks();
        molecule.printInvariants();
        std::cout<< "max_pos is: "<< max_pos << "\n";
        std::vector<int> tmp_neigh_pos = tmp_atoms[max_pos].getAdjacents(); //make a vector of the ID's of the adjacents
        std::vector<Atom> tmp_neigh;    //create a vecotr for nieghbouring elements of tmp_atom[max_pos]
        for(int k = tmp_neigh_pos.size()-1; k >= 0; k--){//create vector of all neighbours of max_pos
            tmp_neigh.push_back(tmp_atoms[tmp_neigh_pos[k]]);
        }
        std::sort(tmp_neigh.begin(), tmp_neigh.end()); //sort it, so the biggest invariant is a tmp_neigh.size() and the smallest at 0
        for(int j = tmp_neigh_pos.size()-1; j >= 0; j--){ //assign ranks to neighbours of max_pos if they don't have one yet
            if(tmp_atoms[tmp_neigh[j].getID()].getRank()==0){ //if the atom, that is a neighbour of max_pos has a rank of 0 (unassigned), assign current rank
                tmp_atoms[tmp_neigh[j].getID()].setRank(Curr_Rank);//these atoms are sorted, so the highest j is the one which will get the lowest rank
                Curr_Rank++;
            }
        }
        molecule.setAtoms(tmp_atoms);
        max_pos = molecule.findNextAtom(tmp_atoms);
        std::cout<< "max_pos is just after the assignment: "<< max_pos << "\n";
    }

    return;
}

//~~~~~~~~~~~~~~~ MORGAN ADAPT ZONE ~~~~~~~~~~~~~~~




Test::Test(){};

int Test::findInvariants(){
    return 1;
}

void Test::setRanks(){
    return;
}    

}