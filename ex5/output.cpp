#include "output.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <utility>

namespace ode{
    bool out::writeOutput(std::vector< std::pair <double, double>>* val){
    std::string filename = "Test_file";
	std::ofstream out(filename); //open and bind output stream

	if(!out) return false; //kill program if output fails
	
	std::cout << "Writing Data to File" << filename << std::endl; //tell user that writing has begun

	for(int n = 0; n < int(val->size()); n++){ 
    out << std::setw(7) << std::get<0>(val->at(n)) << "," << std::get<1>(val->at(n))<<"\n";
	}

	out.close(); //close the output stream

	return true;
    }

}
