//#include "integrator.h"
#include <vector>
#include <utility>

namespace ode{
  class out{
      public:
    bool writeOutput(std::vector< std::pair <double, double>>* val);
};  
}