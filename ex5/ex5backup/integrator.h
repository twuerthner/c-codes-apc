#pragma once
#include "io.h"
#include "function.h"
namespace ode{
// Abstract Parent class integrator
class Integrator{
  protected:
    double stepSize = 1.0;
    
    Function* function;
    
  public: 
    IO* io;
    Integrator(IO* io) : stepSize(io->stepSize), function(io->function), io(io) {};
    Integrator(double stepSize, Function* function) : stepSize(stepSize), function(function){};
    virtual void integrate(double start, double stop, double x0) = 0; //pure virtual function
    virtual void integrate() = 0; //I'm not going to enforce the usage of io in this programm. Is it nice code to have io objects where all data of an input is saved in a single place?
};

//Euler integration, child of Integrator
class EulerIntegrator : public Integrator{
  public:
    EulerIntegrator(IO* io) : Integrator(io) {};
    EulerIntegrator(double stepSize, Function* function) : Integrator(stepSize, function) {};
    void integrate(double start, double stop, double x0);
    void integrate();
};

//Runge-kutta integration, child of Integrator
class RungeKutta4Integrator : public Integrator{
  public:
    RungeKutta4Integrator(IO* io) : Integrator(io) {};
    RungeKutta4Integrator(double stepSize, Function* function) : Integrator(stepSize, function) {};
    void integrate(double start, double stop, double x0);
    void integrate();
};

}

