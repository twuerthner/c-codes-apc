#pragma once
#include "function.h"
#include <iostream>
#include <string>
namespace ode{

class IO{
  public:
    std::string function_name;
    Function* function; 
    double stepSize, start, stop, init;
    IO()=default;
    IO(Function* function) : function(function){};
    IO(Function* function, double stepSize, double start, double stop, double init) : function(function), stepSize(stepSize), start(start), stop(stop), init(init) {};
    void getInput();
};

}
