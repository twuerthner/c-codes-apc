#pragma once
#include <cmath>

namespace ode{

//interface class for all functions used in this exercise
//function classes should have a eval() member function
class Function{
  public:
    virtual double eval(double t, double x) = 0;
};

//Function example of a simple exponential decay
class Exp_decay : public Function{
  public:
    double eval(double t, double x);
};

//Function example of a modulated exponantial growth 
class Mod_grow : public Function{
  public:
    double eval(double t, double x);
};

}
