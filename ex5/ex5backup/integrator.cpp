#include "integrator.h"
namespace ode{
    void EulerIntegrator::integrate(double start, double stop, double x0){

        double x = x0;

        for(double t=start; t<=stop; t+=stepSize){
            std::cout << t << "\t" << x << "\n";
            x = x+stepSize*function->eval(t, x);
        }
        return;
    }

    void EulerIntegrator::integrate(){

        double x = io->init;

        for(double t=io->start; t<=io->stop; t+=io->stepSize){
            std::cout << t << "\t" << x << "\n";
            x = x+io->stepSize*function->eval(t, x);
        }
        return;
    }

    void RungeKutta4Integrator::integrate(double start, double stop, double x0){
        //how do I deal with rewriting this and the IO version of the function?
        double k1, k2, k3, k4;  //actually k_n/h
        double h = stepSize;    //for easier readability
        double y = x0;
        double xk_2_3;          //efficiency
        for(double x = start; x<=stop; x+=h){
            std::cout << x << "\t" << y << "\n";    //output
            xk_2_3=x+h/2;                           //RK-calculation
            k1 = function->eval(x     , y       );
            k2 = function->eval(xk_2_3, y+h*k1/2);
            k3 = function->eval(xk_2_3, y+h*k2/2);
            k4 = function->eval(x+h   , y+h*k3  );
            y += h/6*(k1 + 2*k2 + 2*k3 + k4);
        }
    }
    
    void RungeKutta4Integrator::integrate(){
        double k1, k2, k3, k4;  //actually k_n/h
        double h = io->stepSize;//for easier readability
        double y = io->init;
        double xk_2_3;          //efficiency
        for(double x = io->start; x<=io->stop; x+=h){
            std::cout << x << "\t" << y << "\n";
            xk_2_3=x+h/2;
            k1 = function->eval(x     , y       );
            k2 = function->eval(xk_2_3, y+h*k1/2);
            k3 = function->eval(xk_2_3, y+h*k2/2);
            k4 = function->eval(x+h   , y+h*k3  );
            y += h/6*(k1 + 2*k2 + 2*k3 + k4);
        }
    }
}