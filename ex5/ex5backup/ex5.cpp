#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <string>

#include "io.h"
#include "function.h"
#include "integrator.h"
/*
#include "io.cpp"     wtf is this header stuff about? why does gcc handle it much differently than g++? this is very frustrating
#include "function.cpp"
#include "integrator.cpp"
*/
using namespace ode;

int main(){
  //read in function, stepSize, start, stop, init
  //create instance of function

  IO* io = new IO();
  io->getInput();

  std::cout << io->function_name << "\nEuler \n";
  EulerIntegrator Eulint = EulerIntegrator(io);
  Eulint.integrate();
  //integrate with Euler
  
  
  std::cout << io->function_name << "\nRunge-Kutta \n";
  RungeKutta4Integrator Rukint = RungeKutta4Integrator(io);
  Rukint.integrate();
  //integrate with Runge-Kutta
  
  return 0;
}