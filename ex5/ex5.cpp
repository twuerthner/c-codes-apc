#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <string>

#include "integrator.h"
#include "function.h"
#include "io.h"
#include "output.h"

using namespace ode;

int main(){
  //read in function, stepSize, start, stop, init
  //create instance of function

  IO* io = new IO();
  io->getInput();

  std::cout << io->function_name << "\nEuler \n";
  EulerIntegrator Eulint = EulerIntegrator(io);
  Eulint.integrate();
  //integrate with Euler
  
  
  std::cout << io->function_name << "\nRunge-Kutta \n";
  std::cout << "with parameters: "<< io->function <<" "<< io->stepSize <<" "<< io->start <<" "<<io->stop<<" "<<io->init<<"\n";
  RungeKutta4Integrator Rukint = RungeKutta4Integrator(io);
  Rukint.integrate();
  std::cout << "with parameters: "<< io->function_name <<" "<< io->stepSize <<" "<< io->start <<" "<<io->stop<<" "<<io->init<<"\n";
  //integrate with Runge-Kutta
  out* Ofs = new out();
  std::vector<std::pair<double, double>>* Vals = Rukint.getVals();
  Ofs->writeOutput(Vals);
  return 0;
}