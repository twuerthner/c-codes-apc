#include "io.h"
#include "function.h"
#include <string>
#include <iostream>
namespace ode{
    void IO::getInput(){
        std::cout << "Please type in function_name  stepSize  start  stop  init \n";
        std::cin >> function_name >> stepSize >> start >> stop >> init;
        if(function_name == "grow") function = new Mod_grow;
        else if(function_name == "decay") function = new Exp_decay;
        else if (function_name == "trivial") function = new trivial;
        else throw std::length_error("The function name matches none of the present Functions"); //need to check if legit
    }
}