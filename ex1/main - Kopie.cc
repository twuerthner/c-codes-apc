#include <iostream>
#include <string>
#include <cctype>

namespace exercise{ //would it be more elegant to put this whole namespace thing at the end of the code or is it okay before the main loop?
const int numElements = 4;
std::string lookup(int atomicNumber, std::string t[], int s[]){ //function overloading
  for(int i=0; i <= numElements-1; i++){ //run through all elements of periodic table (if the data set was complete)
    if(atomicNumber==s[i]) return t[i];
  }
  return "Error invalid atomic number"; //returns error if the loop can't match the input to the array
}
int lookup(std::string name, std::string t[], int s[]){
  for(int i=0; i <= numElements-1; i++){
    if(name==t[i]) return s[i];
    }
  std::cout<<"Error invalid atomic name"<<std::endl;
  return -1;
  }
}
using namespace std;
std::string t[]={"H", "Rh", "Cl", "C"};
int s[] = {1, 45, 17, 6};

int main(void){
  string input;
  cin >> input;
  if(isdigit(input[0])){ //checks if first char is digit
    int atomicNumber=stoi(input); 
    cout<<exercise::lookup(atomicNumber, t, s)<<endl; //outputs the atomic name
  }
  else{
    cout<<exercise::lookup(input, t, s)<<endl; //outputs the atomic number
  }
  return 0;
}
 