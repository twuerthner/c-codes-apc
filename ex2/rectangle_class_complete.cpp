#include <iostream>

class rectangle {
 private:
  unsigned int width = 0;
  unsigned int height = 0;
  static unsigned int rect_count;  // rectangle counter
 public:
  const unsigned int MAX_SIZE = 100;                // constant variable
  static unsigned int get_count();                  // get rect_count
  rectangle();                                      // constructor
  rectangle(unsigned int w, unsigned int h);        // constructor
  rectangle(const rectangle &old_rect);             // copy constructor
  ~rectangle() = default;                           // use default destructor
  rectangle &operator=(const rectangle &old_rect);  // assignment operator
  void set_width(unsigned int w);
  unsigned int get_width() const;  // const function
  void set_height(unsigned int h);
  unsigned int get_height() const;
  friend bool equal(const rectangle &r1,
                    const rectangle &r2);  // friend function
};

int main() {
  rectangle myrect(10, 20);
  std::cout << myrect.get_width() << " " << myrect.get_height() << std::endl;
  rectangle myrect2(30, 20);
  std::cout << myrect2.get_width() << " " << myrect2.get_height() << std::endl;
  bool eq = equal(myrect, myrect2);
  std::cout << eq << std::endl;

  const rectangle myrect3(10, 20);
  std::cout << myrect3.get_width() << " " << myrect3.get_height() << std::endl;

  std::cout << "Count: " << rectangle::get_count() << std::endl;
  // look at assignment:
  myrect2 = myrect3;
  std::cout << myrect2.get_width() << " " << myrect2.get_height() << std::endl;
  std::cout << "Count: " << rectangle::get_count() << std::endl;

  // look at assignment to new rectangle:
  rectangle myrect4 = myrect3;
  std::cout << "Count: " << rectangle::get_count() << std::endl;

  return 0;
}

unsigned int rectangle::rect_count = 0;  // no rectangles created yet

unsigned int rectangle::get_count() {  // get rect_count
  return rect_count;
}

rectangle::rectangle() {
  ++rect_count;  // count one up
}

rectangle::rectangle(unsigned int w, unsigned int h) {
  width = std::min(w, MAX_SIZE);
  height = std::min(h, MAX_SIZE);
  ++rect_count;  // count one up
}

rectangle::rectangle(const rectangle &old_rect) {
  width = old_rect.width;
  height = old_rect.height;
  ++rect_count;  // count one up
}

rectangle &rectangle::operator=(const rectangle &old_rect) {
  if (this == &old_rect) {
    return *this;
  }

  width = old_rect.width;
  height = old_rect.height;
  return *this;
}

void rectangle::set_width(unsigned int w) { width = std::min(w, MAX_SIZE); }

void rectangle::set_height(unsigned int h) { height = std::min(h, MAX_SIZE); }

unsigned int rectangle::get_width() const { return width; }

unsigned int rectangle::get_height() const { return height; }

bool equal(const rectangle &r1, const rectangle &r2) {
  if ((r1.width == r2.width) && (r1.height == r2.height)) {
    return true;
  } else {
    return false;
  }
}
