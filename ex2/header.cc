#include<string>
namespace exercises
{
  struct reaction_struct {
    std::string name;
    std::string solvent;
    double temperature;
  };
  
  class stack {
    const static unsigned int MAX_STACK = 20;
    reaction_struct data[MAX_STACK];
    unsigned int top;
    
    public:
      //Constructor
      stack() : top(0) {};
      
      // adds a new reaction to the beginning of the stack
      void push(const reaction_struct &reaction);
      // removes the first element from the stack and returns it
      reaction_struct pop();

      // returns the current size of the stack
      unsigned int get_count() const;
  };

  class queue{
    const static unsigned int MAX_QUEUE = 20;
    reaction_struct data[MAX_QUEUE];
      
    unsigned int head;
    unsigned int tail;
    unsigned int count;
    
    public:
      //Constructor
      queue() : head(0), tail(0), count(0) {};
    
      // adds a new reaction to the end of the queue
      void push(const reaction_struct &reaction);
      // removes the first element from the queue and returns it
      reaction_struct pop();
      
      //getter 
      unsigned int get_count() const;
  };
}

      std::cout << rxn.name <<" "<< rxn.solvent <<" "<< rxn.temperature << std::endl;