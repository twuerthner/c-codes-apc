#include <iostream>
using namespace std;

// elements we want to store in the queue
class element
{
public:
    string name;
};

// minimal linked list (queue)
class node
{
private:
    class element *item;
    class node *next;

public:
    // constructor
    node(); // constructors have the same name as the class
    // and no return value.
    // another constructor, with one argument
    // (using function overloading, also works inside classes)
    node(class node *n);

    // destructor
    ~node();// destructor: beginning with  ̃, no arguments (ever)

    // member functions
    class element *get_item();
    class node *get_next();
    int set_next(class node *n);
};

// implementation of the functions
node::node()
{
    item = NULL;
    next = NULL;
}

node::node(class node *n)
{
    item = NULL;
    next = n;
}

// destructor
node::~node(){
    // if there is a next element, let’s delete it as well
    // this leads to a recursive deletion of the whole queue
    if (next != NULL)
        delete next;
    // and delete the item as well
    if (item != NULL)
        delete item;
}

// member functions
class element *node::get_item()
{
    return item;
}

class node *node::get_next()
{
    return next;
}

int node::set_next(class node *n)
{
    if (next != NULL)
    {
        // we already have a next one, not at the end
        // cannot add here
        return 1;
    }
    next = n;
    return 0;
}

int main(void)
{
    // let’s begin with a queue
    class node *begin = new class node;
    // now of course we would have to add an element to the node
    // we would need additional member functions (like set_element) to do that

    // let’s see how we can grow the queue
    class node *temp = new class node;
    // and here add the element to the node...

    // of course you have to insert at the end of the queue, for a real
    // program, keeping a pointer to the end might be a good idea
    begin->set_next(temp);

    // in the end we can delete the whole queue just like that
    delete begin;

    return 0;
}